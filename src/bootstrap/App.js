import {Navigation} from 'react-native-navigation'
import registerScreens from './screens'

export default class App {
  constructor() {
    registerScreens();
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'HomePage',
        title: 'Sortable List',
      },
      animationType: 'slide-up',
    });
  }
}



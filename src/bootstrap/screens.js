import {Navigation} from 'react-native-navigation';
import VideoTrim from '../modules/components/VideoTrim';
import HomePage from '../modules/components/HomePage';

const screens = {
  HomePage,
  VideoTrim,
};

export default function registerScreens() {
  Object.entries(screens)
    .forEach(([name, screen]) => {
      Navigation.registerComponent(name, () => screen);
    });

}
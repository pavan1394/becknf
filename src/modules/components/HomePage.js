import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Button
} from 'react-native';
import SortableList from 'react-native-sortable-list';
import Box from './Box';

const window = Dimensions.get('window');

const data = [
  {
    id: 1,
    title: 'Box A',
  },
  {
    id: 2,
    title: 'Box B',
  },
  {
    id: 3,
    title: 'Box C',
  },
  {
    id: 4,
    title: 'Box D',
  },
  {
    id: 5,
    title: 'Box E',
  },
];


export default class HomePage extends Component {

  static navigationOptions = {
    title: 'Sortable List',
  };

  renderRow = ({data, active}) => {
    return <Box data={data} active={active} />
  };

  goToVideoTrim = () => {
    this.props.navigator.push({
      screen: 'VideoTrim',
      title: 'Video Trim',
    });
  };


  render() {
    return (
      <View style={styles.container}>
        <SortableList
              horizontal
              style={styles.list}
              data={data}
              renderRow={this.renderRow} />
        <Button
          title="Video Trim"
          onPress={() => this.goToVideoTrim()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },

  list: {
    height: 210,
    width: window.width,
  },
});

import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  NativeModules
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
// import ImagePicker from 'react-native-customized-image-picker';
import { VideoPlayer, Trimmer, ProcessingManager } from 'react-native-video-processing';
import If from '../../helpers/If';
const { RNTrimmerManager: TrimmerManager } = NativeModules;

export default class VideoTrim extends Component {

	static navigationOptions = {
    title: 'Video Trim',
  };

	state = {
    videoSource: null,
    startTime: null,
    endTime:null,
  };

  componentWillUnmount() {
  	console.log('received unmount');
  }

  selectVideoTapped = async () => {
  	const options = {
  title: 'Select Video',
  takePhotoButtonTitle: '',
  chooseFromLibraryButtonTitle: 'Choose from Gallery',
  mediaType: 'video',
  storageOptions: {
    skipBackup: true,
    path: 'Videos',
  },
};
    await ImagePicker.showImagePicker(options, (response) => {

      if (response.didCancel) {
        console.log('User cancelled video picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else {
      	console.log('path', response);
      	this.setPath('file://'+response.path);
       const opts = {
            mediaType: 'video',
            videoQuality: 'medium',
            storageOptions: {
              skipBackup: true,
              path: 'videos',
           }
        };
      }
    });
  }

  setPath = (source) => {
    if (source) {
    this.setState({videoSource: source});
    }
  };

  setTrimOptions = (e) => {
    if(!this.state.videoSource) {
      alert('Please select a video to trim');
    } else {
  	console.log('Trimmer time', e);
    if (e && e.endTime) {
      this.setState({
        startTime: e.startTime,
        endTime: e.endTime,
      });
    } else {
          alert('Please drag on video to select trim duration');
        }
  }
}

    trimVideo = () => {
      if(!this.state.videoSource) {
      alert('Please select a video to trim');
    } else {
      if (this.state.endTime) {
        const options = {
            startTime: this.state.startTime,
            endTime: this.state.endTime,
            storageOptions: {
          skipBackup: true,
          path: 'videos',
          },
        };

        this.videoPlayerRef.trim(options)
            .then((data) => {
            if (data) {
              alert('Trimed video is saved at this path ' + data)
            } else {
              alert('Video trim is failed');
            }
          });

        // ProcessingManager.trim(this.state.videoSource, options) 
        //   .then((data) => {
        //     if (data) {
        //       alert('Trimed video is saved at this path' + data)
        //     } else {
        //       alert('Video trim is failed');
        //     }
        // });
      }
    } 
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
      <If condition={this.state.videoSource}>
      {() => (
      <View>
        <VideoPlayer
            ref={ref => this.videoPlayerRef = ref}
            play={true}     
            replay={false}   
            rotate={true}   
            source={this.state.videoSource}
            style={{ height: '50%', width: '100%', marginBottom: 20}}
            resizeMode={VideoPlayer.Constants.resizeMode.STRETCH}
            onChange={({ nativeEvent }) => console.log(nativeEvent)} 
        />
        <Trimmer
            source={this.state.videoSource}
            height={100}
            width={300}
            style={{marginTop: 10}}
            onChange={(e) => {
            	console.log('Trimmer', e);
            	this.setTrimOptions(e);
            }}
        />
        </View>
      )}
      </If>
         <View style={{flex:1, flexDirection: 'row', padding: 10, alignItems: 'center', justifyContent: 'center'}}>
         <Button
              onPress={() => {
              	this.selectVideoTapped();
              }}
              testID={'SelectVideo'}
              color="#841584"
              title="Select Video"
          />
          <View style={{flex: 1}}/>
         <Button
              onPress={() => {
                this.trimVideo();
              }}
              testID={'TrimVideo'}
              color="#841584"
              title="Trim Video"
          /> 
         </View>
         </View>
    );
  }
}

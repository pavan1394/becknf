import React, {Component} from 'react';
import 'react-native';
import HomePage from '../src/modules/components/HomePage';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
	const home = renderer.create(
		<HomePage/>
	).toJSON();
	expect( home ).toMatchSnapshot();
});
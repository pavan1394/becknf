import React, {Component} from 'react';
import 'react-native';
import VideoTrim from '../src/modules/components/VideoTrim';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

it('disables the button element', () => {
  const wrapper = shallow(<BaseButton />);
  const buttonElement = wrapper.find('TrimVideo');
  console.log('wrapper', wrapper);
  expect(buttonElement.props().disabled).toBe(true);
});

it('does not render the button element when [certain state is true]', () => {
  expect(wrapper.find('TrimVideo')).toHaveLength(0);
});